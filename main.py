import logging
from agent import *
from component import *
from utils import *
import argparse
from math import log10,floor
#from fast_cts import CTSDensityModel


parser = argparse.ArgumentParser(description='main')
parser.add_argument('-t','--task',type=str,default='Pong')
parser.add_argument('-m', '--mode' , type = str , default= 'momentdqn')
parser.add_argument('-mh' , '--moment_hidden' , nargs='+' , type=int,default=[512])
# parser.add_argument('-mn' , '--moment_num' , type=int, default=1)
parser.add_argument('-var' , '--var' , type=float, default=-1.0)
# parser.add_argument('-mb' , '--moment_beta' , nargs='+' , type= float ,default=[1])
parser.add_argument('-s', '--suff' , type=str , default='1')
parser.add_argument('-eb', '--exploration_beta' , type=float , default=1.0)
parser.add_argument('-mxb' , '--mix_beta' , type = float , default= 1.0)

args = parser.parse_args()
# print(args.moment_hidden)

# def round_sig(x,sig=2):
#     return(round(x,sig-int(floor( log10(abs(x)) ))-1))


# args.moment_beta = [round_sig(float(x)/sum(args.moment_beta)) if x > 0  else 0 for x in args.moment_beta ]
memory_size = 1000000

def async_cart_pole():
    config = Config()
    config.task_fn= lambda: CartPole()
    config.optimizer_fn = lambda params: torch.optim.Adam(params, 0.001)
    config.network_fn = lambda: FCNet([4, 50, 200, 2])
    config.policy_fn = lambda: GreedyPolicy(epsilon=0.5, final_step=5000, min_epsilon=0.1)
    config.worker = OneStepQLearning
    # config.worker = NStepQLearning
    # config.worker = OneStepSarsa
    config.discount = 0.99
    config.target_network_update_freq = 200
    config.max_episode_length = 200
    config.num_workers = 16
    config.update_interval = 6
    config.test_interval = 1
    config.test_repetitions = 50
    config.logger = Logger('./log', gym.logger)
    agent = AsyncAgent(config)
    agent.run()

def a3c_cart_pole():
    config = Config()
    config.task_fn = lambda: CartPole()
    config.optimizer_fn = lambda params: torch.optim.Adam(params, 0.001)
    config.network_fn = lambda: ActorCriticFCNet(4, 2)
    config.policy_fn = SamplePolicy
    config.worker = AdvantageActorCritic
    config.discount = 0.99
    config.max_episode_length = 200
    config.num_workers = 16
    config.update_interval = 6
    config.test_interval = 1
    config.test_repetitions = 30
    config.logger = Logger('./log', gym.logger)
    config.gae_tau = 1.0
    config.entropy_weight = 0.01
    agent = AsyncAgent(config)
    agent.run()

def async_pixel_atari(name):
    config = Config()
    config.history_length = 1
    config.task_fn = lambda: PixelAtari(name, no_op=30, frame_skip=4, frame_size=42)
    task = config.task_fn()
    config.optimizer_fn = lambda params: torch.optim.Adam(params, lr=0.0001)
    config.network_fn = lambda: OpenAIConvNet(
        config.history_length, task.env.action_space.n)
    config.policy_fn = lambda: StochasticGreedyPolicy(
        epsilons=[0.7, 0.7, 0.7], final_step=2000000, min_epsilons=[0.1, 0.01, 0.5],
        probs=[0.4, 0.3, 0.3])
    # config.worker = OneStepSarsa
    # config.worker = NStepQLearning
    config.worker = OneStepQLearning
    config.discount = 0.99
    config.target_network_update_freq = 10000
    config.max_episode_length = 10000
    config.num_workers = 10
    config.update_interval = 20
    config.test_interval = 50000
    config.test_repetitions = 1
    config.logger = Logger('./log', gym.logger)
    agent = AsyncAgent(config)
    agent.run()

def a3c_pixel_atari(name):
    config = Config()
    config.history_length = 1
    config.task_fn = lambda: PixelAtari(name, no_op=30, frame_skip=4, frame_size=42)
    task = config.task_fn()
    config.optimizer_fn = lambda params: torch.optim.Adam(params, lr=0.0001)
    config.network_fn = lambda: OpenAIActorCriticConvNet(
        config.history_length, task.env.action_space.n, LSTM=True)
    config.policy_fn = SamplePolicy
    config.worker = AdvantageActorCritic
    config.discount = 0.99
    config.max_episode_length = 10000
    config.num_workers = 16
    config.update_interval = 20
    config.test_interval = 50000
    config.test_repetitions = 1
    config.logger = Logger('./log/'+name)
    agent = AsyncAgent(config)
    agent.run()

def dqn_fruit():
    config = Config()
    config.task_fn = lambda: Fruit()
    config.optimizer_fn = lambda params: torch.optim.SGD(params, 0.01, momentum=0.9)
    config.reward_weight = np.ones(10) / 10
    config.hybrid_reward = False
    config.network_fn = lambda optimizer_fn: FruitHRFCNet(
        98, 4, config.reward_weight, optimizer_fn)
    config.policy_fn = lambda: GreedyPolicy(epsilon=1.0, final_step=10000, min_epsilon=0.1)
    config.replay_fn = lambda: Replay(memory_size=10000, batch_size=15)
    config.discount = 0.95
    config.target_network_update_freq = 200
    config.max_episode_length = 100
    config.exploration_steps = 200
    config.logger = Logger('./log', gym.logger)
    config.history_length = 1
    config.test_interval = 0
    config.test_repetitions = 10
    config.episode_limit = 5000
    config.double_q = False
    run_episodes(DQNAgent(config))

def hrdqn_fruit():
    config = Config()
    config.task_fn = lambda: Fruit(hybrid_reward=True)
    config.hybrid_reward = True
    config.reward_weight = np.ones(10) / 10
    config.optimizer_fn = lambda params: torch.optim.SGD(params, 0.01, momentum=0.9)
    config.network_fn = lambda optimizer_fn: FruitHRFCNet(
        98, 4, config.reward_weight, optimizer_fn)
    config.policy_fn = lambda: GreedyPolicy(epsilon=1.0, final_step=10000, min_epsilon=0.1)
    config.replay_fn = lambda: HybridRewardReplay(memory_size=10000, batch_size=15)
    config.discount = 0.95
    config.target_network_update_freq = 200
    config.max_episode_length = 100
    config.exploration_steps = 200
    config.logger = Logger('./log', gym.logger)
    config.history_length = 1
    config.test_interval = 0
    config.test_repetitions = 10
    config.target_type = config.expected_sarsa_target
    # config.target_type = config.q_target
    config.double_q = False
    config.episode_limit = 5000
    run_episodes(DQNAgent(config))

def a3c_continuous():
    config = Config()
    config.task_fn = lambda: Pendulum()
    # config.task_fn = lambda: BipedalWalkerHardcore()
    task = config.task_fn()
    config.actor_optimizer_fn = lambda params: torch.optim.Adam(params, 0.0001)
    config.critic_optimizer_fn = lambda params: torch.optim.Adam(params, 0.001)
    config.network_fn = lambda: DisjointActorCriticNet(
        # lambda: GaussianActorNet(task.state_dim, task.action_dim, unit_std=False, action_gate=F.tanh, action_scale=2.0),
        lambda: GaussianActorNet(task.state_dim, task.action_dim, unit_std=True),
        lambda: GaussianCriticNet(task.state_dim))
    config.policy_fn = lambda: GaussianPolicy()
    config.worker = ContinuousAdvantageActorCritic
    config.discount = 0.99
    config.max_episode_length = task.max_episode_steps
    config.num_workers = 8
    config.update_interval = 20
    config.test_interval = 1
    config.test_repetitions = 1
    config.entropy_weight = 0
    config.gradient_clip = 40
    config.logger = Logger('./log', gym.logger)
    agent = AsyncAgent(config)
    agent.run()

def p3o_continuous():
    config = Config()
    config.task_fn = lambda: Pendulum()
    # config.task_fn = lambda: BipedalWalker()
    # config.task_fn = lambda: BipedalWalkerHardcore()
    # config.task_fn = lambda: Roboschool('RoboschoolInvertedPendulum-v1')
    # config.task_fn = lambda: Roboschool('RoboschoolAnt-v1')
    task = config.task_fn()
    config.actor_network_fn = lambda: GaussianActorNet(task.state_dim, task.action_dim,
                                                       gpu=False, unit_std=True)
    config.critic_network_fn = lambda: GaussianCriticNet(task.state_dim, gpu=False)
    config.network_fn = lambda: DisjointActorCriticNet(config.actor_network_fn, config.critic_network_fn)
    config.actor_optimizer_fn = lambda params: torch.optim.Adam(params, 0.001)
    config.critic_optimizer_fn = lambda params: torch.optim.Adam(params, 0.001)

    config.policy_fn = lambda: GaussianPolicy()
    config.replay_fn = lambda: GeneralReplay(memory_size=2048, batch_size=2048)
    config.worker = ProximalPolicyOptimization
    config.discount = 0.99
    config.gae_tau = 0.97
    config.num_workers = 6
    config.test_interval = 1
    config.test_repetitions = 1
    config.max_episode_length = task.max_episode_steps
    config.entropy_weight = 0
    config.gradient_clip = 20
    config.rollout_length = 10000
    config.optimize_epochs = 1
    config.ppo_ratio_clip = 0.2
    config.logger = Logger('./log', gym.logger)
    agent = AsyncAgent(config)
    agent.run()

def d3pg_continuous():
    config = Config()
    # config.task_fn = lambda: Pendulum()
    # config.task_fn = lambda: ContinuousLunarLander()
    # config.task_fn = lambda: Roboschool('RoboschoolInvertedPendulum-v1')
    config.task_fn = lambda: Roboschool('RoboschoolReacher-v1')
    # config.task_fn = lambda: BipedalWalker()
    task = config.task_fn()
    config.actor_network_fn = lambda: DeterministicActorNet(
        task.state_dim, task.action_dim, F.tanh, 2, non_linear=F.relu, batch_norm=False)
    config.critic_network_fn = lambda: DeterministicCriticNet(
        task.state_dim, task.action_dim, non_linear=F.relu, batch_norm=False)
    config.network_fn = lambda: DisjointActorCriticNet(config.actor_network_fn, config.critic_network_fn)
    config.actor_optimizer_fn = lambda params: torch.optim.Adam(params, lr=1e-4)
    config.critic_optimizer_fn =\
        lambda params: torch.optim.Adam(params, lr=1e-4)
    config.replay_fn = lambda: SharedReplay(memory_size=1000000, batch_size=64,
                                            state_shape=(task.state_dim, ), action_shape=(task.action_dim, ))
    config.discount = 0.99
    config.max_episode_length = task.max_episode_steps
    config.random_process_fn = \
        lambda: OrnsteinUhlenbeckProcess(size=task.action_dim, theta=0.15, sigma=0.2,
                                         n_steps_annealing=100000)
    config.worker = DeterministicPolicyGradient
    config.num_workers = 6
    config.min_memory_size = 50
    config.target_network_mix = 0.001
    config.test_interval = 500
    config.test_repetitions = 1
    config.gradient_clip = 20
    config.logger = Logger('./log', gym.logger)
    agent = AsyncAgent(config)
    agent.run()

def dqn_pixel_atari(name):
    config = Config()
    config.gradient_clip = 1
    config.traning_freq = 4
    config.history_length = 4
    config.task_fn = lambda: PixelAtari(name, no_op=30, frame_skip=4, normalized_state=False)
    config.test_task_fn  = lambda: test_PixelAtari(name, no_op=30, frame_skip=4, normalized_state=False)
    config.action_dim = action_dim = config.task_fn().action_dim
    config.optimizer_fn = lambda params: torch.optim.RMSprop(params, lr=0.00025, alpha=0.95, eps=0.01)
    config.network_fn = lambda optimizer_fn: NatureConvNet(config.history_length, action_dim, optimizer_fn)
    config.episode_limit = 10000000
    config.total_step_limit  = 100*100*10000
    # config.network_fn = lambda optimizer_fn: DuelingNatureConvNet(config.history_length, n_actions, optimizer_fn)
    config.policy_fn = lambda: GreedyPolicy(epsilon=1.0, final_step=1000000, min_epsilon=0.1)
    config.replay_fn = lambda: Replay(memory_size=memory_size, batch_size=32, dtype=np.uint8)
    config.discount = 0.99
    config.target_network_update_freq = 10000
    config.max_episode_length = 0

    config.exploration_steps= 50000
    # config.logname = '_'.join([name , args.mode , str(len(args.moment_hidden)) ]
    #                           + [str(x) for x in args.moment_hidden]
    #                           +  [ str(args.var), str(args.exploration_beta) , str(args.mix_beta),args.suff] )
    # config.logger = Logger('./log_moments/'+config.logname+'.txt' )
    # config.model_save_root = './model_save'
    config.logname = '_'.join([name , args.mode , str(len(args.moment_hidden)) ]
                              + [str(x) for x in args.moment_hidden]
                              +  [ str(args.var), str(args.exploration_beta) , str(args.mix_beta),args.suff] )

    # config.logname = '_'.join([name , args.mode , str(len(args.moment_hidden)) , str(args.moment_num)]+[str(x) for x in args.moment_hidden] +    [str(x) for x in args.moment_beta]+[args.suff])

    if not os.path.exists('./log_iue_new/'):
        os.mkdir('./log_iue_new/')
    if not os.path.exists( './model_save_iue_new'):
        os.mkdir( './model_save_iue_new')
    config.logger = Logger('./log_iue_new/'+config.logname+'.txt' )
    config.model_save_root = './model_save_iue_new'

    print(config.logname)
    config.test_interval = 250000
    # config.test_repetitions = 50
    # config.double_q = True
    config.double_q = False
    run_episodes(DQNAgent(config))

def dqn_pixel_atari_moment(name):
    moments_num = args.moment_num
    hidden_dim = args.moment_hidden
    # print('!!!!', hidden_dim)

    config = Config()
    config.gradient_clip = 1
    config.traning_freq = 4
    config.moments_num = moments_num
    config.moments_beta = args.moment_beta
    assert len(config.moments_beta) == moments_num
    # assert len(hidden_dim) == moments_num
    config.history_length = 4
    config.task_fn = lambda: PixelAtari(name, no_op=30, frame_skip=4, normalized_state=False)
    config.test_task_fn  = lambda: test_PixelAtari(name, no_op=30, frame_skip=4, normalized_state=False)
    action_dim = config.task_fn().action_dim
    # print('!!!!!',action_dim)
    config.optimizer_fn = lambda params: torch.optim.RMSprop(params, lr=0.00025, alpha=0.95, eps=0.01)
    config.network_fn = lambda optimizer_fn: NatureConvNet_moment(config.history_length,
                                                                  action_dim, moments_num, hidden_dim, optimizer_fn)
    config.episode_limit = 10000000
    # config.network_fn = lambda optimizer_fn: DuelingNatureConvNet(config.history_length, n_actions, optimizer_fn)
    config.policy_fn = lambda: GreedyPolicy(epsilon=1.0, final_step=1000000, min_epsilon=0.1)
    config.replay_fn = lambda: Replay(memory_size=memory_size, batch_size=32, dtype=np.uint8)
    config.discount = 0.99
    config.target_network_update_freq = 10000
    config.max_episode_length = 0
    config.exploration_steps= 50000
    # config.logname = '_'.join([name , args.mode]+[str(x) for x in args.moment_hidden] + [str(x) for x in args.moment_beta]+[args.suff])
    config.logname = '_'.join(
        [name, args.mode, str(len(args.moment_hidden)), str(args.moment_num)] + [str(x) for x in args.moment_hidden] + [
            str(x) for x in args.moment_beta] + [args.suff])
    config.logger = Logger('./log_moments/'+config.logname+'.txt' )
    config.model_save_root = './model_save'
    print(config.logname)
    print(args.__dict__)
    config.test_interval = 250000
    # config.test_repetitions = 50
    # config.double_q = True
    config.double_q = False
    run_episodes(DQNMomentAgent(config))

def dqn_pixel_atari_IUE(name):

    NatureConvNet_m  = NatureConvNet

    config = Config()
    config.gradient_clip = 1
    config.traning_freq = 4
    config.history_length = 4
    config.task_fn = lambda: PixelAtari(name, no_op=30, frame_skip=4, normalized_state=False)
    config.test_task_fn  = lambda: test_PixelAtari(name, no_op=30, frame_skip=4, normalized_state=False)
    config.action_dim = action_dim = config.task_fn().action_dim
    config.optimizer_fn = lambda params: torch.optim.RMSprop(params, lr=0.00025, alpha=0.95, eps=0.01)
    config.optimizer_fn_m = lambda params: torch.optim.RMSprop(params, lr=0.00025, alpha=0.95, eps=0.01)

    config.network_fn = lambda optimizer_fn: NatureConvNet(config.history_length, action_dim, optimizer_fn)
    config.network_fn_m = lambda optimizer_fn : NatureConvNet_m(config.history_length, action_dim, optimizer_fn)
    config.episode_limit = 10000000
    config.total_step_limit  = 100*100*10000
    # config.network_fn = lambda optimizer_fn: DuelingNatureConvNet(config.history_length, n_actions, optimizer_fn)
    config.policy_fn = lambda: GreedyPolicy(epsilon=1.0, final_step=1000000, min_epsilon=0.1)
    config.replay_fn = lambda: Replay(memory_size=memory_size, batch_size=32, dtype=np.uint8)
    config.discount = 0.99
    config.target_network_update_freq = 10000
    config.target_network_m_update_freq = 10000
    #exploration
    config.exploration_beta = args.exploration_beta
    config.density_model = CTSDensityModel
    config.density_model_args = {
        'height' : 42,
        'width' : 42,
        'num_bins' :  8,
        'beta' : 1.0
    }
    config.mix_beta=args.mix_beta
    config.var = args.var
    config.max_episode_length = 0
    config.exploration_steps= 50000
    #log
    config.logname = '_'.join([name , args.mode , str(len(args.moment_hidden)) ]
                              + [str(x) for x in args.moment_hidden]
                              +  [ str(args.var), str(args.exploration_beta) , str(args.mix_beta),args.suff] )

    # config.logname = '_'.join([name , args.mode , str(len(args.moment_hidden)) , str(args.moment_num)]+[str(x) for x in args.moment_hidden] +    [str(x) for x in args.moment_beta]+[args.suff])
    if not os.path.exists('./log_iue_new/'):
        os.mkdir('./log_iue_new/')
    if not os.path.exists( './model_save_iue_new'):
        os.mkdir( './model_save_iue_new')
    config.logger = Logger('./log_iue_new/'+config.logname+'.txt' )
    config.model_save_root = './model_save_iue_new'

    print(config.logname)
    config.test_interval = 250000
    config.double_q = False

    run_episodes(DQNAgent_IUE_change_reward(config))
    # run_episodes(DQNAgent_IUE_change_reward_no_count(config))

# def dqn_pixel_atari_IUE(name):
#
#     NatureConvNet_m  = NatureConvNet
#
#     config = Config()
#     config.gradient_clip = 1
#     config.traning_freq = 4
#     config.history_length = 4
#     config.task_fn = lambda: PixelAtari(name, no_op=30, frame_skip=4, normalized_state=False)
#     config.test_task_fn  = lambda: test_PixelAtari(name, no_op=30, frame_skip=4, normalized_state=False)
#     config.action_dim = action_dim = config.task_fn().action_dim
#     config.optimizer_fn = lambda params: torch.optim.RMSprop(params, lr=0.00025, alpha=0.95, eps=0.01)
#     config.optimizer_fn_m = lambda params: torch.optim.RMSprop(params, lr=0.00025, alpha=0.95, eps=0.01)
#
#     config.network_fn = lambda optimizer_fn: NatureConvNet(config.history_length, action_dim, optimizer_fn)
#     config.network_fn_m = lambda optimizer_fn : NatureConvNet_m(config.history_length, action_dim, optimizer_fn)
#     config.episode_limit = 10000000
#     # config.network_fn = lambda optimizer_fn: DuelingNatureConvNet(config.history_length, n_actions, optimizer_fn)
#     config.policy_fn = lambda: GreedyPolicy(epsilon=1.0, final_step=1000000, min_epsilon=0.1)
#     config.replay_fn = lambda: Replay(memory_size=memory_size, batch_size=32, dtype=np.uint8)
#     config.discount = 0.99
#     config.target_network_update_freq = 10000
#     config.target_network_m_update_freq = 10000
#
#     config.exploration_beta = args.exploration_beta
#     config.density_model = CTSDensityModel
#     config.density_model_args = {
#         'height' : 42,
#         'width' : 42,
#         'num_bins' :  8,
#         'beta' : 0.05
#     }
#
#     config.max_episode_length = 0
#     config.exploration_steps= 50000
#     args.moment_beta = [args.exploration_beta]
#     config.logname = '_'.join([name , args.mode , str(len(args.moment_hidden)) , str(args.moment_num)]+[str(x) for x in args.moment_hidden] +    [str(x) for x in args.moment_beta]+[args.suff])
#     config.logger = Logger('./log_moments/'+config.logname+'.txt' )
#     config.model_save_root = './model_save'
#     print(config.logname)
#     config.test_interval = 250000
#     # config.test_repetitions = 50
#     # config.double_q = True
#     config.double_q = False
#     run_episodes(DQNAgent_IUE_change_reward(config))

def dqn_cart_pole(name):
    config = Config()
    config.task_fn = lambda: CartPole(name)
    config.test_task_fn = lambda: CartPole(name)
    config.optimizer_fn = lambda params: torch.optim.RMSprop(params, 0.001)
    config.network_fn = lambda optimizer_fn: FCNet([4, 24, 48, 2], optimizer_fn)
    # config.network_fn = lambda optimizer_fn: FCNet_moment(4, 2, [24], 1 , [48],
                                                          # optimizer_fn)

    # config.network_fn = lambda optimizer_fn: DuelingFCNet([8, 50, 200, 2], optimizer_fn)
    config.policy_fn = lambda: GreedyPolicy(epsilon=1.0, final_step=2000, min_epsilon=0.1)
    config.replay_fn = lambda: Replay(memory_size=10000, batch_size=10)
    config.discount = 0.99
    config.target_network_update_freq = 200
    config.max_episode_length = 210
    config.exploration_steps = 500
    config.logname = '_'.join([name , args.mode , str(len(args.moment_hidden)) ]
                              + [str(x) for x in args.moment_hidden]
                              +  [ str(args.var), str(args.exploration_beta) , str(args.mix_beta),args.suff] )

    config.logger = Logger('./log_moments/'+config.logname+'.txt' )
    print(config.logname)
    config.model_save_root = './model_save_simple_dqn'
    config.total_step_limit = 10000
    config.traning_freq = 1
    config.history_length = 1
    config.test_interval = 1000
    config.test_repetitions = 50

    # config.double_q = True
    config.double_q = False
    run_episodes(DQNAgent(config))

def dqn_cart_pole_moment(name):
    hidden_dim = [24 ]
    moments_num = args.moment_num
    moments_dim = args.moment_hidden

    config = Config()
    config.moments_num = moments_num
    config.moments_beta = args.moment_beta
    assert len(config.moments_beta) == moments_num
    config.task_fn = lambda: CartPole(name)
    config.test_task_fn = lambda: CartPole(name)
    config.optimizer_fn = lambda params: torch.optim.RMSprop(params, 0.001)

    input_dim = config.task_fn().state_dim
    action_dim = config.task_fn().action_dim
    # config.network_fn = lambda optimizer_fn: FCNet([4, 24, 48, 2], optimizer_fn)
    config.network_fn = lambda optimizer_fn: FCNet_moment(input_dim, action_dim,hidden_dim , moments_num , moments_dim, optimizer_fn,False)
    # config.network_fn = lambda optimizer_fn: DuelingFCNet([8, 50, 200, 2], optimizer_fn)
    config.policy_fn = lambda: GreedyPolicy(epsilon=1.0, final_step=2000, min_epsilon=0.1)
    config.replay_fn = lambda: Replay(memory_size=10000, batch_size=10)
    config.discount = 0.99
    config.target_network_update_freq = 200
    config.max_episode_length = 200
    config.exploration_steps = 1000
    logname = '_'.join([name , args.mode]+[str(x) for x in args.moment_hidden] + [str(x) for x in args.moment_beta]+[args.suff])
    config.logger = Logger('./log_moments/'+logname+'.txt' )
    print(logname)
    config.history_length = 1
    config.test_interval = 10
    config.test_repetitions = 50
    # config.double_q = True
    config.double_q = False
    # run_episodes(DQNAgent(config))

    run_episodes(DQNMomentAgent(config))



if __name__ == '__main__':
    # gym.logger.setLevel(logging.DEBUG)
    # gym.logger.setLevel(logging.INFO)
    task_name = args.task+'NoFrameskip-v4'

    if args.mode == 'momentdqn':
        main_fn = dqn_pixel_atari_moment
    elif args.mode == 'dqn':
        main_fn = dqn_pixel_atari
    elif args.mode == 'a3c':
        main_fn = a3c_pixel_atari
    elif args.mode == 'simpledqn':
        main_fn = dqn_cart_pole
        task_name = args.task + '-v0'
    elif args.mode == 'momentsimpledqn':
        main_fn = dqn_cart_pole_moment
        task_name = args.task +'-v0'
    elif args.mode == 'dqn_iue':
        main_fn = dqn_pixel_atari_IUE

    else :
        print('!!!!!!!!!!!!!!error mode')
        assert 1==2
    main_fn(task_name)


    # dqn_cart_pole()
    # async_cart_pole()
    # a3c_cart_pole()
    # a3c_continuous()
    # p3o_continuous()
   # d3pg_continuous()

    # dqn_fruit()
    # hrdqn_fruit()

    # dqn_pixel_atari('PongNoFrameskip-v4')
    # async_pixel_atari('PongNoFrameskip-v4')
    # a3c_pixel_atari('PongNoFrameskip-v4')

    # dqn_pixel_atari('BreakoutNoFrameskip-v4')
    # dqn_pixel_atari('PongNoFrameskip-v4')
    # dqn_pixel_atari('MsPacmanNoFrameskip-v4')
    # dqn_pixel_atari('QbertNoFrameskip-v4')
    # dqn_pixel_atari('CrazyClimberNoFrameskip-v4')
    # dqn_pixel_atari('BattleZoneNoFrameskip-v4')

    # async_pixel_atari('BreakoutNoFrameskip-v4')
    # a3c_pixel_atari('BreakoutNoFrameskip-v4')
    # dqn_pixel_atari('PongNoFrameskip-v4')
    # dqn_pixel_atari_moment('MsPacmanNoFrameskip-v4')

