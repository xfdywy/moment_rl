import logging
from agent import *
from component import *
from utils import *
import argparse
import numpy as np
import torch
import copy

# parser = argparse.ArgumentParser(description='main')
# parser.add_argument('-t','--task',type=str,default='Pong')
# parser.add_argument('-m', '--mode' , type = str , default= 'momentdqn')
# parser.add_argument('-mh' , '--moment_hidden' , nargs='+' , type=int,default=[512])
# parser.add_argument('-mn' , '--moment_num' , type=int, default=1)
# parser.add_argument('-mb' , '--moment_beta' , nargs='+' , type= float ,default=[1])
#
# args = parser.parse_args()

gpu = True

np.random.seed(1)
torch.manual_seed(1)
torch.cuda.manual_seed_all(1)


config={'history_length' : 4 ,
        'action_dim' : 6,
        'optimizer_fn' : lambda params: torch.optim.RMSprop(params, lr=0.00025, alpha=0.95, eps=0.01),
        'moments_num' : 1,
        'hidden_dim' : [512]
        }

record_raw = {
            'dqn':
                { 'param' : [],
                  'loss' : [] ,
                  'output' : [],
                  'grad' : [],
                  'state' : []
                },

            'moment_dqn' :
                {'param': [],
                 'loss': [],
                 'output': [],
                 'grad': [],
                 'state' :[]
                 },
          }


record_all = []


record = copy.deepcopy(record_raw)

def to_torch_variable(self, x, dtype='float32'):
    if isinstance(x, Variable):
        return x
    if not isinstance(x, torch.FloatTensor):
        x = torch.from_numpy(np.asarray(x, dtype=dtype))
    if self.gpu:
        # print('!!!!!!!!!!!!')
        pass
        x = x.cuda()
    return Variable(x)

def run_test_dqn():
    np.random.seed(1)
    torch.manual_seed(1)
    torch.cuda.manual_seed_all(1)

    network_fn = lambda optimizer_fn: NatureConvNet(config['history_length'], config['action_dim'],
                                                    optimizer_fn,gpu)

    learning_network = network_fn(config['optimizer_fn'])
    target_network = network_fn(config['optimizer_fn'])
    target_network.load_state_dict(learning_network.state_dict())

    temp = []
    for params in learning_network.parameters():
        temp.append(params.data.cpu().view(-1).numpy())
    record['dqn']['param'].append(np.concatenate(temp))

    # conv2 = learning_network.state_dict()['conv2.weight'].cpu().numpy()[0, 0, :, :].copy().reshape(-1)
    # fc5 = learning_network.state_dict()['fc5.weight'].cpu().numpy()[:, 0 ].copy().reshape(-1)
    # record['dqn']['param'].append(conv2)
    # record['dqn']['param'].append(fc5)


    states = np.random.random([2,4,84,84])*1000+1000
    rewards= [500,500]
    actions = [1,2]
    terminals = [False,False]
    next_states = np.random.random([2,4,84,84])*1000+1000

    actions = learning_network.to_torch_variable(actions, 'int64').unsqueeze(1)
    terminals = learning_network.to_torch_variable(terminals)
    rewards = learning_network.to_torch_variable(rewards)

    # print('states and next states' , states,next_states)
    for ii in range(1):
        q_next = target_network.predict(next_states, False).detach()

        q_next, _ = q_next.max(1)
        # print(q_next , terminals)

        q_next =  q_next #* (1 - terminals)
        # print(q_next , terminals)
        # q_next.add_(rewards)

        q =  learning_network.predict(states, False)
        q = q.gather(1, actions).squeeze(1)
        loss = nn.MSELoss()(q, q_next + rewards)

        learning_network.zero_grad()
        loss.backward()
        learning_network.optimizer.step()

    temp=[]
    for params in learning_network.parameters():
        temp.append(params.data.cpu().view(-1).numpy())
    record['dqn']['param'].append(np.concatenate(temp))

    record['dqn']['loss'].append(loss.data.cpu().numpy().squeeze())
    record['dqn']['output'].append(q.data.cpu().numpy().squeeze())
    record['dqn']['output'].append(q_next.data.cpu().numpy().squeeze())
    record['dqn']['state'].append(states.squeeze())

    temp = []
    for params in learning_network.parameters():
        temp.append(params.grad.data.cpu().view(-1).numpy())

    record['dqn']['grad'].append(np.concatenate(temp))
    #

def run_test_dqnmoment():

    np.random.seed(1)
    torch.manual_seed(1)
    torch.cuda.manual_seed_all(1)

    network_fn = lambda optimizer_fn: NatureConvNet_moment(config['history_length'], config['action_dim'],
                                                         config['moments_num'],
                                                        config['hidden_dim'], optimizer_fn,gpu)

    learning_network = network_fn(config['optimizer_fn'])
    target_network = network_fn(config['optimizer_fn'])
    target_network.load_state_dict(learning_network.state_dict())

    # conv2 = learning_network.state_dict()['conv2.weight'].cpu().numpy()[0, 0, :, :].copy().reshape(-1)
    # fc5 = learning_network.state_dict()['moments_layer.0.2.weight'].cpu().numpy()[:, 0].copy().reshape(-1)
    # record['moment_dqn']['param'].append(conv2)
    # record['moment_dqn']['param'].append(fc5)

    temp = []
    for params in learning_network.parameters():
        temp.append(params.data.cpu().view(-1).numpy())
    record['moment_dqn']['param'].append(np.concatenate(temp))

    states = np.random.random([2,4,84,84])*1000+1000
    rewards= [500,500]
    actions = [1,2]
    terminals = [False,False]
    next_states = np.random.random([2,4,84,84])*1000+1000



    actions =  learning_network.to_torch_variable(actions, 'int64').unsqueeze(1)
    terminals =  learning_network.to_torch_variable(terminals)
    rewards =  learning_network.to_torch_variable(rewards)

    for ii in range(1):

        q = compute_current_value(learning_network=learning_network ,
                              states= states,
                              actions=actions,
                              moments_num=config['moments_num'])
        q_next = compute_next_value(target_network = target_network ,
                           next_states=next_states,
                           terminals=terminals,
                           moments_num=config['moments_num'])

        loss = compute_loss(q=q,q_next=q_next,rewards=rewards , moments_num=config['moments_num'])
        learning_network.zero_grad()
        loss.backward()
        learning_network.optimizer.step()

        # q_next = target_network.predict(next_states, False).detach()
        # q_next.squeeze(2)
        #
        # q_next, _ = q_next.max(1)
        #
        #
        #
        # q_next =  q_next #* (1 - terminals)
        # # print(q_next)
        # # q_next.add_(rewards)
        #
        # q =  learning_network.predict(states, False)
        # q = q.squeeze(2)
        # q = q.gather(1, actions).squeeze(1)
        #
        # loss = nn.MSELoss()(q, q_next + rewards.view([2,1]))
        #
        # learning_network.zero_grad()
        # loss.backward()
        # learning_network.optimizer.step()


    temp = []
    for params in learning_network.parameters():
        temp.append(params.data.cpu().view(-1).numpy())
    record['moment_dqn']['param'].append(np.concatenate(temp))

    record['moment_dqn']['loss'].append(loss.data.cpu().numpy().squeeze())
    record['moment_dqn']['output'].append(q.data.cpu().numpy().squeeze())
    record['moment_dqn']['output'].append(q_next.data.cpu().numpy().squeeze())
    record['moment_dqn']['state'].append(states.squeeze())

    temp = []
    for params in learning_network.parameters():
        temp.append(params.grad.data.cpu().view(-1).numpy())

    record['moment_dqn']['grad'].append(np.concatenate(temp))


def compute_current_value( learning_network,states, actions , moments_num):


    # batch size X action num X moments num
    q_all =  learning_network.predict(states, False)

    # batch size X 1 X 1
    ind = actions.unsqueeze(2)

    # batch size X 1 X moments_num
    ind = ind.expand(ind.size(0), 1,  moments_num)

    # batch_size X 1 X moments_number
    return (q_all.gather(1, ind))


def compute_next_value( target_network,next_states, terminals , moments_num):
    q_next_all = target_network.predict(next_states, False).detach()

    argmax_a = q_next_all[:, :, 0].max(dim=1, keepdim=True)[1]
    # print('!!!!!' , argmax_a.size())
    argmax_a = argmax_a.unsqueeze(2)

    argmax_a = argmax_a.expand(argmax_a.size(0), 1, moments_num)

    next_value = q_next_all.gather(1, argmax_a)
    terminals = terminals.view(-1, 1, 1)
    terminals = terminals.expand(terminals.size(0), 1,   moments_num)
    terminals = 1 - terminals
    return (next_value * terminals)


def compute_loss( q, q_next, rewards , moments_num):
    moments_beta = [1]
    loss = 0
    for ii in range(1, moments_num + 1):
        temp = 0
        for jj in range(ii + 1):
            if jj == 0:
                label_next_value = 1
            else:
                label_next_value = q_next[:, :, jj - 1]
            label = float(comb(ii, jj)) * (rewards.unsqueeze(1) ** (ii - jj)) * (
                (1) ** jj) * label_next_value
            temp += label

        # a =
        # print(a.data.cpu().numpy(), ii)
        loss +=  moments_beta[ii - 1] * nn.MSELoss()(q[:, :, ii - 1], temp)

    return (loss)

print(record)
run_test_dqn()
run_test_dqnmoment()
print(np.max(np.abs(record['dqn']['loss'][0] - record['moment_dqn']['loss'][0])),
np.max(np.abs(record['dqn']['output'][0] - record['moment_dqn']['output'][0])),
np.max(np.abs(record['dqn']['output'][1] - record['moment_dqn']['output'][1])),
np.max(np.abs(record['dqn']['grad'][0] - record['moment_dqn']['grad'][0])),
np.max(np.abs(record['dqn']['param'][0] - record['moment_dqn']['param'][0])),
np.max(np.abs(record['dqn']['param'][1] - record['moment_dqn']['param'][1])),
np.max(np.abs(record['dqn']['state'][0] - record['moment_dqn']['state'][0])),
      )