import logging

import matplotlib.pyplot as plt
from agent import *
from component import *
from utils import *
import argparse
import time
import pickle
import os


parser = argparse.ArgumentParser(description='eval')
parser.add_argument('-t','--task',type=str,default=None)
parser.add_argument('-d' , '--model_save_root' , type=str , default='model_save_iue_new')
parser.add_argument('-m', '--mode' , type = str , default= None)
parser.add_argument('-mh' , '--moment_hidden' , nargs='+' , type=int,default=[512])
parser.add_argument('-var' , '--var' , nargs = '+' ,  type=float, default=None)
parser.add_argument('-eb', '--exploration_beta' ,nargs = '+' , type=float , default=None)
parser.add_argument('-mxb' , '--mix_beta' , nargs = '+' ,type = float , default= None)
parser.add_argument('-s', '--suff' , nargs = '+', type=str , default=None)
parser.add_argument('-r', '--rep' , type=int , default='125000')
parser.add_argument('-oon', '--oldornew' , type=str , default='n')
parser.add_argument('-td' , '--test_dir' , type=str , default='test_result')

args = parser.parse_args()

task_name = args.task+ 'NoFrameskip-v4'
config = Config()

config.history_length = 4
config.model_save_root = args.model_save_root
config.test_task_fn  = lambda: test_PixelAtari(task_name, no_op=30, frame_skip=4, normalized_state=False)
action_dim = config.test_task_fn().action_dim
config.test_task = config.test_task_fn()


def episode(config, max_step):
    task = config.test_task
    model = config.model
    # episode_start_time = time.time()
    state =  task.reset()
    history_buffer = [np.zeros_like(state)] *  config.history_length
    history_buffer.pop(0)
    history_buffer.append(state)
    state = np.vstack( history_buffer)
    total_reward = 0.0
    steps = 0
    while True:
        value = model.predict(np.stack([ task.normalize_state(state)]), False)
        value = value.cpu().data.numpy().flatten()
        action = np.argmax(value)
        next_state, reward, done, info =  task.step(action)
        True_reward = info[-1]
        done = (done or (max_step and steps > max_step))
        history_buffer.pop(0)
        history_buffer.append(next_state)
        next_state = np.vstack(history_buffer)
        total_reward += np.sum(True_reward  )
        steps += 1
        state = next_state
        if done:
            break
    return(steps , total_reward)

def eval_model(config):
    step_last = args.rep
    all_rewards =[]
    n=0
    while step_last>0:

        start_time = time.time()
        step , reward = episode(config,step_last)
        all_rewards.append(reward)
        step_last -= step
        n+=1
        end_time = time.time()
        print(n,step , step_last , reward , float(step) / (end_time - start_time))
    print(len(all_rewards))
    if len(all_rewards)>1:
        return( np.mean(all_rewards[:-1]) , np.std(all_rewards[:-1]))
    else:
        return (np.mean(all_rewards), np.std(all_rewards ))


if  not os.path.exists(args.test_dir):
    os.mkdir(args.test_dir)
test_dir_txt = os.path.join(args.test_dir , 'text')
test_dir_pkl = os.path.join(args.test_dir , 'pkl')
test_dir_img = os.path.join(args.test_dir , 'img')

for ii  in [test_dir_txt , test_dir_pkl, test_dir_img] :
    if not os.path.exists(ii):
        os.mkdir(ii)


all_setting = os.listdir(os.path.join(config.model_save_root , task_name))
for this_setting in all_setting:
    loginfo = this_setting.split('_')
    now_ind = 0
    env_name = loginfo[0]

    if loginfo[2] == 'iue':
        mode = 'dqn_iue'
        now_ind = 3
    else :
        mode = 'dqn'
        now_ind = 2

    moment_hidden_num = int(loginfo[now_ind])
    now_ind +=1
    moment_hidden = loginfo[now_ind : now_ind+moment_hidden_num]
    now_ind += moment_hidden_num
    var = float(loginfo[now_ind])
    now_ind+=1
    eb = float(loginfo[now_ind])
    now_ind +=1
    mxb = float(loginfo[now_ind])
    now_ind +=1
    suff = loginfo[now_ind]
    assert now_ind == len(loginfo)-1
    # print(this_setting,all_setting)

    if args.var is not None:
        if var not in args.var:
            continue
    if args.exploration_beta  is not None:
        if eb not in args.exploration_beta:
            # print(eb , args.exploration_beta)
            continue
    if args.mix_beta is not None:
        if mxb not in args.exploration_beta:
            continue
    if args.suff  is not None:
        if suff not in args.suff:
            continue



    model_path_root =  config.model_save_root
    model_path = model_path_root

    if not os.path.exists(model_path):
        print('error! no ', model_path)

    model_path_env = env_name
    model_path = os.path.join(model_path, model_path_env)
    if not os.path.exists(model_path):
        os.mkdir(model_path)

    model_path_setting = this_setting

    model_path = os.path.join(model_path, model_path_setting)

    if not os.path.exists(model_path):
        print('error! no ', model_path)


    config.network_fn = lambda optimizer_fn: NatureConvNet(config.history_length, action_dim, optimizer_fn)
    config.model = config.network_fn(None)


    allmodel = os.listdir(model_path)
    allmodel = [x for x in allmodel if '.pkl' not in x and '.txt' not in x]

    mean_reward = {}
    std_reward = {}
    allmodel_info = [[x,int(x.split('_')[-1])] for x in allmodel]
    allmodel_info.sort(key=lambda x:x[1])
    allmodel_info.reverse()

    txt_file_path = os.path.join(test_dir_txt , '_'.join([model_path_setting ,'result.txt']))
    pkl_file_path = os.path.join(test_dir_pkl , '_'.join([model_path_setting ,'result.pkl']))
    img_file_path = os.path.join(test_dir_img , '_'.join([model_path_setting ,'result.png']))


    print('start evaluate ' + this_setting )

    with open(txt_file_path, 'wb') as txt_f:
        for this_model_info in allmodel_info:
            time_step = this_model_info[1]
            this_model = this_model_info[0]
            config.model.load_state_dict(torch.load(os.path.join(model_path,this_model)))

            mean_temp , std_temp = eval_model(config)
            mean_reward[time_step] = mean_temp
            std_reward[time_step] = std_temp
            print('time step:%d, mean reward:%f, std reward:%f'%(time_step , mean_temp , std_temp))
            txt_f.write('time step:%d, mean reward:%f, std reward:%f'%(time_step , mean_temp , std_temp))
            txt_f.write('\n')
            txt_f.flush()


    with open( pkl_file_path , 'wb') as f:
        pickle.dump([mean_reward , std_reward] , f)

    # plt.plot(mean_reward.keys() , mean_reward.values() ,  )
    # plt.title(model_path_setting)
    # # plt.show()
    # plt.savefig(img_file_path , dpi=200)

