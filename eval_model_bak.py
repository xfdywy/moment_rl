import logging
from agent import *
from component import *
from utils import *
import argparse
import time
import pickle
import matplotlib.pyplot as plt

# parser = argparse.ArgumentParser(description='eval')
# parser.add_argument('-t','--task',type=str,default='Pong')
# parser.add_argument('-m', '--mode' , type = str , default= 'dqn')
# parser.add_argument('-mh' , '--moment_hidden' , nargs='+' , type=int,default=[512])
# parser.add_argument('-mn' , '--moment_nu10rm' , type=int, default=1)
# parser.add_argument('-mb' , '--moment_beta' , nargs='+' , type= float ,default=[1])
# parser.add_argument('-s', '--suff' , type=str , default='1')
# parser.add_argument('-r', '--rep' , type=int , default='125000')
# parser.add_argument('-oon', '--oldornew' , type=str , default='n')
#
#
# args = parser.parse_args()





# task_name = args.task+ 'NoFrameskip-v4'
task_name = 'Venture'+ 'NoFrameskip-v4'
config = Config()

config.history_length = 4

config.test_task_fn  = lambda: test_PixelAtari(task_name, no_op=30, frame_skip=4, normalized_state=False)
action_dim = config.test_task_fn().action_dim
config.network_fn = lambda optimizer_fn: NatureConvNet(config.history_length, action_dim, optimizer_fn)
config.model = config.network_fn(None)
config.test_task = config.test_task_fn()
config.model_save_root = './model_save'
# if args.oldornew == 'o':
#     config.logname = '_'.join([task_name , args.mode , str(len(args.moment_hidden)) , str(args.moment_num)]+[str(x) for x in args.moment_hidden] +    [str(x) for x in args.moment_beta]+[args.suff])
# elif args.oldornew == 'n':
#     config.logname = '_'.join(
#         [task_name, args.mode, str(len(args.moment_hidden)), str(args.var), str(args.exploration_beta), str(args.mix_beta),
#          args.suff])
# else:
#     print('error old or new!')
#     assert  1==2


config.logname = 'VentureNoFrameskip-v4_dqn_iue_1_1_512_0.5_53'
loginfo =  config.logname.split('_')

env_name = loginfo[0]
# mode = loginfo[1]
# moment_num = loginfo[2]
# moment_hidden = loginfo[3:3 + moment_num]
# moment_beta = loginfo[3 + moment_num: 3 + 2 * moment_num]
suff = loginfo[-1]
model_path_root =  config.model_save_root
model_path = model_path_root

if not os.path.exists(model_path):
    print('error! no ', model_path)

model_path_env = env_name
model_path = os.path.join(model_path, model_path_env)
if not os.path.exists(model_path):
    os.mkdir(model_path)

model_path_setting = '_'.join(loginfo)
# print(1,model_path_setting)
model_path = os.path.join(model_path, model_path_setting)
# print(2,model_path)
if not os.path.exists(model_path):
    print('error! no ', model_path)

allmodel = os.listdir(model_path)
allmodel = [x for x in allmodel if '.pkl' not in x and '.txt' not in x]

for nii,ii in enumerate(allmodel):
    print(nii,ii)


def eval_model(config):

    # step_last = args.rep
    step_last = 100000
    all_rewards =[]
    while step_last>0:
        step , reward = episode(config,step_last)
        all_rewards.append(reward)
        step_last -= step
        print('!!!!!!!!')
    print(len(all_rewards))
    if len(all_rewards)>1:
        return( np.mean(all_rewards[:-1]) , np.std(all_rewards[:-1]))
    else:
        return (np.mean(all_rewards), np.std(all_rewards ))



def episode(config, max_step):

    task = config.test_task
    model = config.model

    episode_start_time = time.time()
    state =  task.reset()
    history_buffer = [np.zeros_like(state)] *  config.history_length


    history_buffer.pop(0)
    history_buffer.append(state)
    state = np.vstack( history_buffer)
    total_reward = 0.0
    steps = 0
    while True:


        value = model.predict(np.stack([ task.normalize_state(state)]), False)
        value = value.cpu().data.numpy().flatten()
        action = np.argmax(value)

        next_state, reward, done, info =  task.step(action)
        True_reward = info[-1]
        if(True_reward > 0 ):
            print(steps , True_reward)
        if steps % 1000 == 0:
            print(steps)
        done = (done or (max_step and steps > max_step))
        history_buffer.pop(0)
        history_buffer.append(next_state)
        next_state = np.vstack(history_buffer)
        total_reward += np.sum(True_reward  )
        steps += 1
        state = next_state
        if done:
            print(steps,'done')
            break

    return(steps , total_reward)

mean_reward = {}
std_reward = {}

this_model = allmodel[14]
with open(os.path.join(model_path , 'result.txt') , 'wb') as txt_f:
    for this_model in allmodel:
        time_step = int(this_model.split('_')[-1])

        config.model.load_state_dict(torch.load(os.path.join(model_path,this_model)))

        mean_temp , std_temp = eval_model(config)
        mean_reward[time_step] = mean_temp
        std_reward = std_temp
        print('time step:%d, mean reward:%f, std reward:%f'%(time_step , mean_temp , std_temp))
        txt_f.write('time step:%d, mean reward:%f, std reward:%f'%(time_step , mean_temp , std_temp))


with open(os.path.join(model_path , 'result.pkl') , 'wb') as f:
    pickle.dump([mean_reward , std_reward] , f)
plt.plot(mean_reward.keys() , mean_reward.values() , '.')
plt.title(model_path_setting)
plt.show()
