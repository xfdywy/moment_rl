#######################################################################
# Copyright (C) 2017 Shangtong Zhang(zhangshangtong.cpp@gmail.com)    #
# Permission given to modify the code as long as you keep this        #
# declaration at the top                                              #
#######################################################################

import numpy as np
import torch
import random
import torch.multiprocessing as mp





class Replay:
    def __init__(self, memory_size, batch_size, history_length=4,  dtype=np.uint8):
        print('history_length' , history_length)
        self.memory_size = memory_size
        self.history_length = history_length
        self.batch_size = batch_size
        self.dtype = dtype

        self.states = None
        self.actions = np.zeros(self.memory_size, dtype=np.int8)
        self.rewards = np.zeros(self.memory_size,dtype=np.float32)
        # self.next_states = None
        self.terminals = np.zeros(self.memory_size, dtype=np.int8)

        self.bottom = 0
        self.top = 0
        self.size = 0
        self.full = False


    def feed(self, experience):
        state, action, reward, next_state, done = experience
        state = state[-1]
        # next_state = next_state[-1]

        if self.states is None:
            self.state_shape = state.shape
            # print(self.state_shape)
            self.states = np.empty((self.memory_size, ) + self.state_shape, dtype=self.dtype)

            # self.next_states = np.empty((self.memory_size, ) + state.shape, dtype=self.dtype)

        self.states[self.top] = state
        self.actions[self.top] = action
        self.rewards[self.top] = reward
        # self.next_states[self.pos][:] = next_state
        self.terminals[self.top] = done

        if self.size == self.memory_size:
            self.full = True
            self.bottom = (self.bottom + 1) % self.memory_size
        else:
            self.size += 1
        self.top = (self.top + 1) % self.memory_size





    def sample(self):
        # upper_bound = self.memory_size if self.full else self.pos
        # sampled_indices = np.random.randint(0, upper_bound, size=self.batch_size)
        img = np.zeros((self.batch_size,
                         self.history_length + 1,) + self.state_shape,
                        dtype='uint8')
        action = np.zeros((self.batch_size ), dtype=np.int8)
        reward = np.zeros((self.batch_size ), dtype=np.float32)
        terminal = np.zeros((self.batch_size ), dtype=np.int8)

        count = 0
        while count < self.batch_size:
            # Randomly choose a time step from the replay memory.
            index = np.random.randint(self.bottom,
                                     self.bottom + self.size - self.history_length)

            # Both the before and after states contain phi_length
            # frames, overlapping except for the first and last.
            all_indices = np.arange(index, index + self.history_length + 1)
            end_index = index + self.history_length - 1

            # Check that the initial state corresponds entirely to a
            # single episode, meaning none but its last frame (the
            # second-to-last frame in imgs) may be terminal. If the last
            # frame of the initial state is terminal, then the last
            # frame of the transitioned state will actually be the first
            # frame of a new episode, which the Q learner recognizes and
            # handles correctly during training by zeroing the
            # discounted future reward estimate.
            if np.any(self.terminals.take(all_indices[0:-2], mode='wrap')):
                continue

            # Add the state transition to the response.
            # print(self.states.shape , img.shape)
            img[count] = self.states.take(all_indices, axis=0, mode='wrap')
            action[count] = self.actions.take(end_index, mode='wrap')
            reward[count] = self.rewards.take(end_index, mode='wrap')
            terminal[count] = self.terminals.take(end_index, mode='wrap')
            count += 1
        return [img[:,:-1,:,:] , action,reward,img[:,1:,:,:], terminal]

        # return [self.states[sampled_indices],
        #         self.actions[sampled_indices],
        #         self.rewards[sampled_indices],
        #         self.next_states[sampled_indices],
        #         self.terminals[sampled_indices]]

class HybridRewardReplay:
    def __init__(self, memory_size, batch_size, dtype=np.float32):
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.dtype = dtype

        self.states = None
        self.actions = np.empty(self.memory_size, dtype=np.int8)
        self.rewards = None
        self.next_states = None
        self.terminals = np.empty(self.memory_size, dtype=np.int8)

        self.pos = 0
        self.full = False


    def feed(self, experience):
        state, action, reward, next_state, done = experience

        if self.states is None:
            self.rewards = np.empty((self.memory_size, ) + reward.shape, dtype=self.dtype)
            self.states = np.empty((self.memory_size, ) + state.shape, dtype=self.dtype)
            self.next_states = np.empty((self.memory_size, ) + state.shape, dtype=self.dtype)

        self.states[self.pos][:] = state
        self.actions[self.pos] = action
        self.rewards[self.pos][:] = reward
        self.next_states[self.pos][:] = next_state
        self.terminals[self.pos] = done

        self.pos += 1
        if self.pos == self.memory_size:
            self.full = True
            self.pos = 0

    def sample(self):
        upper_bound = self.memory_size if self.full else self.pos
        sampled_indices = np.random.randint(0, upper_bound, size=self.batch_size)
        return [self.states[sampled_indices],
                self.actions[sampled_indices],
                self.rewards[sampled_indices],
                self.next_states[sampled_indices],
                self.terminals[sampled_indices]]

class SharedReplay:
    def __init__(self, memory_size, batch_size, state_shape, action_shape):
        self.memory_size = memory_size
        self.batch_size = batch_size

        self.states = torch.zeros((self.memory_size, ) + state_shape)
        self.actions = torch.zeros((self.memory_size, ) + action_shape)
        self.rewards = torch.zeros(self.memory_size)
        self.next_states = torch.zeros((self.memory_size, ) + state_shape)
        self.terminals = torch.zeros(self.memory_size)

        self.states.share_memory_()
        self.actions.share_memory_()
        self.rewards.share_memory_()
        self.next_states.share_memory_()
        self.terminals.share_memory_()

        self.pos = 0
        self.full = False
        self.buffer_lock = mp.Lock()

    def feed_(self, experience):
        state, action, reward, next_state, done = experience
        self.states[self.pos][:] = torch.FloatTensor(state)
        self.actions[self.pos][:] = torch.FloatTensor(action)
        self.rewards[self.pos] = reward
        self.next_states[self.pos][:] = torch.FloatTensor(next_state)
        self.terminals[self.pos] = done

        self.pos += 1
        if self.pos == self.memory_size:
            self.full = True
            self.pos = 0

    def size(self):
        if self.full:
            return self.memory_size
        return self.pos

    def sample_(self):
        upper_bound = self.memory_size if self.full else self.pos
        sampled_indices = torch.LongTensor(np.random.randint(0, upper_bound, size=self.batch_size))
        return [self.states[sampled_indices],
                self.actions[sampled_indices],
                self.rewards[sampled_indices],
                self.next_states[sampled_indices],
                self.terminals[sampled_indices]]

    def feed(self, experience):
        with self.buffer_lock:
            self.feed_(experience)

    def sample(self):
        with self.buffer_lock:
            return self.sample_()

class HighDimActionReplay:
    def __init__(self, memory_size, batch_size, dtype=np.float32):
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.dtype = dtype

        self.states = None
        self.actions = None
        self.rewards = np.empty(self.memory_size)
        self.next_states = None
        self.terminals = np.empty(self.memory_size, dtype=np.int8)

        self.pos = 0
        self.full = False


    def feed(self, experience):
        state, action, reward, next_state, done = experience

        if self.states is None:
            self.states = np.empty((self.memory_size, ) + state.shape, dtype=self.dtype)
            self.actions = np.empty((self.memory_size, ) + action.shape)
            self.next_states = np.empty((self.memory_size, ) + state.shape, dtype=self.dtype)

        self.states[self.pos][:] = state
        self.actions[self.pos][:] = action
        self.rewards[self.pos] = reward
        self.next_states[self.pos][:] = next_state
        self.terminals[self.pos] = done

        self.pos += 1
        if self.pos == self.memory_size:
            self.full = True
            self.pos = 0

    def size(self):
        if self.full:
            return self.memory_size
        return self.pos

    def sample(self):
        upper_bound = self.memory_size if self.full else self.pos
        sampled_indices = np.random.randint(0, upper_bound, size=self.batch_size)
        return [self.states[sampled_indices],
                self.actions[sampled_indices],
                self.rewards[sampled_indices],
                self.next_states[sampled_indices],
                self.terminals[sampled_indices]]

class GeneralReplay:
    def __init__(self, memory_size, batch_size):
        self.buffer = []
        self.memory_size = memory_size
        self.batch_size = batch_size

    def feed(self, experiences):
        for experience in zip(*experiences):
            self.buffer.append(experience)
            if len(self.buffer) > self.memory_size:
                del self.buffer[0]

    def sample(self):
        sampled = zip(*random.sample(self.buffer, self.batch_size))
        return sampled

    def clear(self):
        self.buffer = []

    def full(self):
        return len(self.buffer) == self.memory_size
