#######################################################################
# Copyright (C) 2017 Shangtong Zhang(zhangshangtong.cpp@gmail.com)    #
# Permission given to modify the code as long as you keep this        #
# declaration at the top                                              #
#######################################################################

from network import *
from component import *
from utils import *
import numpy as np
import time
import os
import pickle
import torch

class DQNAgent:
    def __init__(self, config):
        self.config = config
        self.learning_network = config.network_fn(config.optimizer_fn)
        self.target_network = config.network_fn(config.optimizer_fn)
        self.target_network.load_state_dict(self.learning_network.state_dict())
        self.train_task = config.task_fn()
        self.test_task = config.test_task_fn()
        self.replay = config.replay_fn()
        self.policy = config.policy_fn()
        self.total_steps = 0
        self.history_buffer = None

    def compute_current_value(self , states,actions):

        #batch size X action num
        q_all = self.learning_network.predict(states, False)

        #batch size X 1
        ind = actions

        #batch_size X 1
        return(q_all.gather(1,ind))

    def compute_next_value(self,next_states , terminals   ):
        #batch size x action num
        q_next_all = self.target_network.predict(next_states, False).detach()
        if self.config.double_q:
            print('!!!!!error')

        #batch size x 1
        argmax_a = q_next_all.max(dim=1 , keepdim = True)[1]

        #batch_size X 1
        next_value = q_next_all.gather(1 , argmax_a )
        terminals = terminals.view(-1,1)
        # terminals = terminals.expand(terminals.size(0),1,self.config.moments_num)
        terminals = 1-terminals
        return(  next_value * terminals,argmax_a)

    def compute_loss_q(self, q, q_next ,rewards):

        rewards = rewards.view(-1,1)
        label = self.config.discount * q_next + rewards

        return(self.learning_network.criterion(q, label))

    def episode(self, deterministic=False):
        if deterministic:
            self.task = self.test_task
        else:
            self.task = self.train_task

        episode_start_time = time.time()
        state = self.task.reset()
        if self.history_buffer is None:
            self.history_buffer = [np.zeros_like(state)] * self.config.history_length
        else:
            self.history_buffer.pop(0)
            self.history_buffer.append(state)
        state = np.vstack(self.history_buffer)
        total_reward = 0.0
        steps = 0
        record = []
        while True:
            if self.config.test_interval and self.total_steps % self.config.test_interval == 0:
                self.save_model()

            value = self.learning_network.predict(np.stack([self.task.normalize_state(state)]), False)
            value = value.cpu().data.numpy().flatten()
            if deterministic:
                action = np.argmax(value)
            elif self.total_steps < self.config.exploration_steps:
                action = np.random.randint(0, len(value))
            else:
                action = self.policy.sample(value)
            next_state, reward, done, info = self.task.step(action)
            True_reward = info[-1]
            done = (done or (self.config.max_episode_length and steps > self.config.max_episode_length))
            self.history_buffer.pop(0)
            self.history_buffer.append(next_state)
            next_state = np.vstack(self.history_buffer)
            if not deterministic:
                self.replay.feed([state, action, reward, next_state, int(done)])
                self.total_steps += 1
            total_reward += np.sum(True_reward * self.config.reward_weight)
            steps += 1
            state = next_state
            if done:
                break
            if not deterministic and self.total_steps > self.config.exploration_steps and self.total_steps % self.config.traning_freq ==0:
                experiences = self.replay.sample()
                states, actions, rewards, next_states, terminals = experiences
                states = self.task.normalize_state(states)
                next_states = self.task.normalize_state(next_states)
                terminals = self.learning_network.to_torch_variable(terminals)
                rewards = self.learning_network.to_torch_variable(rewards)
                actions = self.learning_network.to_torch_variable(actions, 'int64').unsqueeze(1)

                if self.config.hybrid_reward:
                    q_next = self.target_network.predict(next_states, True)
                    target = []
                    for q_next_ in q_next:
                        if self.config.target_type == self.config.q_target:
                            target.append(q_next_.detach().max(1)[0])
                        elif self.config.target_type == self.config.expected_sarsa_target:
                            target.append(q_next_.detach().mean(1))
                    target = torch.stack(target, dim=1).detach()
                    terminals = self.learning_network.to_torch_variable(terminals).unsqueeze(1)
                    rewards = self.learning_network.to_torch_variable(rewards)
                    target = self.config.discount * target * (1 - terminals)
                    target.add_(rewards)
                    q = self.learning_network.predict(states, True)
                    q_action = []
                    actions = self.learning_network.to_torch_variable(actions, 'int64').unsqueeze(1)
                    for q_ in q:
                        q_action.append(q_.gather(1, actions))
                    q_action = torch.cat(q_action, dim=1)
                    loss = self.learning_network.criterion(q_action, target)
                else:

                    q0 =                 self.compute_current_value(states=states, actions=actions)
                    q1,argmax_a =        self.compute_next_value(next_states=next_states , terminals=terminals)
                    loss_q =             self.compute_loss_q(q0, q1,rewards)
                    self.learning_network.zero_grad()
                    loss_q.backward()
                    for param in self.learning_network.parameters():
                        param.grad.data.clamp_(-1*self.config.gradient_clip,1*self.config.gradient_clip)

                    self.learning_network.optimizer.step()
            if not deterministic and self.total_steps % self.config.target_network_update_freq == 0:
                self.target_network.load_state_dict(self.learning_network.state_dict())
            if not deterministic and self.total_steps > self.config.exploration_steps:
                self.policy.update_epsilon()
        episode_time = time.time() - episode_start_time
        self.config.logger.debug('episode steps %d, episode time %f, time per step %f' %
                          (steps, episode_time, episode_time / float(steps)))



        return total_reward, steps



    def save(self, file_name):
        with open(file_name, 'wb') as f:
            pickle.dump(self.learning_network.state_dict(), f)
    def save_model(self):
        print(self.config.test_interval, self.total_steps)
        if self.config.test_interval and self.total_steps % self.config.test_interval == 0:
            self.config.logger.info('save model...')
            loginfo =  self.config.logname.split('_')
            # print(loginfo,'!!!!!!')
            env_name = loginfo[0]
            # mode = loginfo[1]
            # moment_num = loginfo[2]
            # moment_hidden = loginfo[3:3 + moment_num]
            # moment_beta = loginfo[3 + moment_num: 3 + 2 * moment_num]
            suff = loginfo[-1]
            model_path_root = self.config.model_save_root
            model_path = model_path_root

            if not os.path.exists(model_path):
                os.mkdir(model_path)

            model_path_env = env_name
            model_path = os.path.join(model_path, model_path_env)
            if not os.path.exists(model_path):
                os.mkdir(model_path)

            model_path_setting =   '_'.join(loginfo)
            # print(1,model_path_setting)
            model_path = os.path.join(model_path, model_path_setting)
            # print(2,model_path)
            if not os.path.exists(model_path):
                os.mkdir(model_path)

            model_path_file = '_'.join([model_path_setting , str(self.total_steps / 10000)  ])
            # print(3,model_path_file)
            model_path = os.path.join(model_path, model_path_file)
            # print(4,model_path)
            # if not os.path.exists(model_path):
            #     os.mkdir(model_path)

            torch.save(self.learning_network.state_dict(), model_path)

            self.config.logger.info('save model done')

