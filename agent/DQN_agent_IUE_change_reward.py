#######################################################################
# Copyright (C) 2017 Shangtong Zhang(zhangshangtong.cpp@gmail.com)    #
# Permission given to modify the code as long as you keep this        #
# declaration at the top                                              #
#######################################################################

from network import *
from component import *
from utils import *
import numpy as np
import time
import os
import pickle
import torch

class DQNAgent_IUE_change_reward:
    def __init__(self, config):

        self.config = config
        print(self.config.exploration_beta, '!!!')
        self.learning_network = config.network_fn(config.optimizer_fn)
        self.target_network = config.network_fn(config.optimizer_fn)

        self.learning_network_m = config.network_fn(config.optimizer_fn_m)
        self.target_network_m = config.network_fn(config.optimizer_fn_m)

        self.target_network.load_state_dict(self.learning_network.state_dict())
        self.target_network_m.load_state_dict(self.learning_network_m.state_dict())

        self.train_task = config.task_fn()
        self.test_task = config.test_task_fn()
        self.replay = config.replay_fn()
        self.policy = config.policy_fn()
        self.total_steps = 0
        self.history_buffer = None
        self.density_model = config.density_model(**config.density_model_args)
        self.mix_beta = config.mix_beta



    def compute_current_value(self , states,actions):

        #batch size X action num
        q_all = self.learning_network.predict(states, False)

        #batch size X 1
        ind = actions

        #batch_size X 1
        return(q_all.gather(1,ind))

    def compute_next_value(self,next_states , terminals   ):
        #batch size x action num
        q_next_all = self.target_network.predict(next_states, False).detach()
        if self.config.double_q:
            print('!!!!!error')

        #batch size x 1
        argmax_a = q_next_all.max(dim=1 , keepdim = True)[1]

        #batch_size X 1
        next_value = q_next_all.gather(1 , argmax_a )
        terminals = terminals.view(-1,1)
        # terminals = terminals.expand(terminals.size(0),1,self.config.moments_num)
        terminals = 1-terminals
        return(  next_value * terminals * self.mix_beta,argmax_a )

    def compute_current_m(self, states, actions):

        # batch size X action num
        m_all = self.learning_network_m.predict(states, False)

        # batch size X 1
        ind = actions

        # batch_size X 1
        return (m_all.gather(1, ind))

    def compute_next_m(self, next_states, argmax_a , terminals):
        # batch size x action num
        m_next_all = self.target_network_m.predict(next_states, False).detach()
        if self.config.double_q:
            print('!!!!!error')

        # batch_size X 1
        next_m = m_next_all.gather(1, argmax_a)
        terminals = terminals.view(-1, 1)
        # terminals = terminals.expand(terminals.size(0),1,self.config.moments_num)
        terminals = 1 - terminals
        return (next_m * terminals)

    def compute_loss_q(self, q, q_next ,rewards):

        rewards = rewards.view(-1,1)
        label = self.config.discount * q_next + rewards

        return(self.learning_network.criterion(q, label))

    def compute_loss_m(self, m,m_next, q_next ,rewards):

        rewards = rewards.view(-1,1)
        label = rewards**2 + 2* rewards*self.config.discount * q_next + self.config.discount**2 *m_next

        return(self.learning_network.criterion(m, label))


    def episode(self, deterministic=False):
        if deterministic:
            self.task = self.test_task
        else:
            self.task = self.train_task

        episode_start_time = time.time()
        state = self.task.reset()
        if self.history_buffer is None:
            self.history_buffer = [np.zeros_like(state)] * self.config.history_length
        else:
            self.history_buffer.pop(0)
            self.history_buffer.append(state)
        state = np.vstack(self.history_buffer)
        total_reward = 0.0
        steps = 0

        rewards_batch     = list()
        states_batch      = list()
        actions_batch     = list()
        dones_batch       = list()

        max_count = -1
        min_count = 100
        max_std = -1
        min_std  = 100

        while True:
            if self.config.test_interval and self.total_steps % self.config.test_interval == 0:
                self.save_model()

            normalized_state = np.stack([self.task.normalize_state(state)])

            value = self.learning_network.predict(normalized_state, False)
            value = value.cpu().data.numpy().flatten()
            m = self.learning_network_m.predict(normalized_state , False)
            m = m.cpu().data.numpy().flatten()

            assert  len(value) == self.config.action_dim
            assert len(m) == self.config.action_dim


            #selection action
            # print(normalized_state[0,-1,...].shape)
            # print(self.density_model)

            # print('!!!!',pseudocount)

            if deterministic:
                action = np.argmax(value)
            elif self.total_steps < self.config.exploration_steps:
                action = np.random.randint(0, self.config.action_dim)
            else:
                # action = self.policy.sample(value , m ,pseudocount , self.config.exploration_beta)
                action = self.policy.sample(value)
            #env step
            next_state, reward, done, info = self.task.step(action)
            True_reward = info[-1]
            done = (done or (self.config.max_episode_length and steps > self.config.max_episode_length))
            self.history_buffer.pop(0)
            self.history_buffer.append(next_state)
            next_state = np.vstack(self.history_buffer)

            normalized_state_next = np.stack([self.task.normalize_state(next_state)])
            bonus = self.density_model.update(normalized_state_next[0,-1,...])
            pseudocount = (1.0/ bonus)**2-0.01

            if self.config.var == -1:

                variance = np.clip((m - value ** 2), 1e-5, 1e4)
            else :
                variance =  [self.config.var]*self.config.action_dim
            # print()
            std = np.sqrt(variance)

            # std = std / std.sum()
            # std = np.clip()
            # print(variance,std,action,'###########')
            std_this = std[action]
            # bonus = self.config.exploration_beta * 0.01 /(0.01 + std_this / np.sqrt(pseudocount + 0.01))


            bonus = self.config.exploration_beta * 1.0/ (1.0 +  0.1*std_this * np.sqrt(pseudocount + 0.0001))


            reward_temp = reward
            reward = np.clip(reward + bonus , -1,1)
            # print(self.total_steps)
            max_count = max(max_count , pseudocount)
            max_std = max(max_std , std_this)
            min_count = min(min_count , pseudocount)
            min_std = min(min_std , std_this)
            # if self.total_steps % 100 == 0 :
            #     print('!!!!!!!' , self.total_steps , reward, reward_temp ,
            #           bonus, std_this,pseudocount , [max_count , min_count , max_std , min_std])
            # if  bonus>1:
            #     print('$$$$$$' , self.total_steps,reward, reward_temp ,
            #           bonus, std_this,pseudocount,[max_count , min_count , max_std , min_std])
            if not deterministic:
                states_batch.append(state)
                actions_batch.append(action)
                rewards_batch.append(reward)
                dones_batch.append(int(done))
                # self.replay.feed([state, action, reward, next_state, int(done)])
                self.total_steps += 1


            total_reward += np.sum(True_reward * self.config.reward_weight)
            steps += 1
            state = next_state
            if done:
                episode_length = len(rewards_batch)
                mc_returns = np.zeros(episode_length , dtype=np.float32)
                running_total = 0.0
                for i,r in enumerate(reversed(rewards_batch)):
                    running_total = r + self.config.discount * running_total
                    mc_returns[episode_length-i-1] = running_total
                mixed_returns = self.config.mix_beta * np.asarray(rewards_batch)\
                                + (1-self.config.mix_beta)*mc_returns

                for i in range(episode_length):
                    self.replay.feed([states_batch[i], actions_batch[i],
                                      mixed_returns[i], states_batch[0], dones_batch[i]])

                break

            # learning
            if not deterministic and self.total_steps > self.config.exploration_steps and self.total_steps % self.config.traning_freq ==0:
                experiences     = self.replay.sample()
                states, actions, rewards, next_states, terminals = experiences
                states          = self.task.normalize_state(states)
                next_states     = self.task.normalize_state(next_states)
                terminals       = self.learning_network.to_torch_variable(terminals)
                rewards         = self.learning_network.to_torch_variable(rewards)
                actions         = self.learning_network.to_torch_variable(actions, 'int64').unsqueeze(1)

                if self.config.hybrid_reward:
                    print('!!!!!! error')
                    q_next = self.target_network.predict(next_states, True)
                    target = []
                    for q_next_ in q_next:
                        if self.config.target_type == self.config.q_target:
                            target.append(q_next_.detach().max(1)[0])
                        elif self.config.target_type == self.config.expected_sarsa_target:
                            target.append(q_next_.detach().mean(1))
                    target = torch.stack(target, dim=1).detach()
                    terminals = self.learning_network.to_torch_variable(terminals).unsqueeze(1)
                    rewards = self.learning_network.to_torch_variable(rewards)
                    target = self.config.discount * target * (1 - terminals)
                    target.add_(rewards)
                    q = self.learning_network.predict(states, True)
                    q_action = []
                    actions = self.learning_network.to_torch_variable(actions, 'int64').unsqueeze(1)
                    for q_ in q:
                        q_action.append(q_.gather(1, actions))
                    q_action = torch.cat(q_action, dim=1)
                    loss = self.learning_network.criterion(q_action, target)
                else:

                    q =                 self.compute_current_value(states=states, actions=actions)
                    q_next,argmax_a =   self.compute_next_value(next_states=next_states , terminals=terminals)
                    m =                 self.compute_current_m(states=states,actions=actions)
                    m_next =            self.compute_next_m(next_states=next_states,argmax_a=argmax_a,terminals=terminals)

                    loss_q =            self.compute_loss_q(q, q_next,rewards)
                    loss_m =            self.compute_loss_m(m,m_next,q_next,rewards)

                self.learning_network.zero_grad()
                self.learning_network_m.zero_grad()
                loss_q.backward()
                loss_m.backward()
                for param in self.learning_network.parameters():
                    param.grad.data.clamp_(-1*self.config.gradient_clip,1*self.config.gradient_clip)
                for param in self.learning_network_m.parameters():
                    param.grad.data.clamp_(-1 * self.config.gradient_clip, 1 * self.config.gradient_clip)

                self.learning_network.optimizer.step()
                self.learning_network_m.optimizer.step()
            if not deterministic and self.total_steps % self.config.target_network_update_freq == 0:
                self.target_network.load_state_dict(self.learning_network.state_dict())
            if not deterministic and self.total_steps % self.config.target_network_m_update_freq == 0:
                self.target_network_m.load_state_dict(self.learning_network_m.state_dict())

            if not deterministic and self.total_steps > self.config.exploration_steps:
                self.policy.update_epsilon()
        episode_time = time.time() - episode_start_time
        self.config.logger.debug('episode steps %d, episode time %f, time per step %f' %
                          (steps, episode_time, episode_time / float(steps)))



        return total_reward, steps



    def save(self, file_name):
        with open(file_name, 'wb') as f:
            pickle.dump(self.learning_network.state_dict(), f,protocol=2)
    def save_model(self):
        print(self.config.test_interval, self.total_steps)
        if self.config.test_interval and self.total_steps % self.config.test_interval == 0:
            self.config.logger.info('save model...')
            loginfo =  self.config.logname.split('_')
            # print(loginfo,'!!!!!!')
            env_name = loginfo[0]
            # mode = loginfo[1]
            # moment_num = loginfo[2]
            # moment_hidden = loginfo[3:3 + moment_num]
            # moment_beta = loginfo[3 + moment_num: 3 + 2 * moment_num]
            suff = loginfo[-1]
            model_path_root = self.config.model_save_root
            model_path = model_path_root

            if not os.path.exists(model_path):
                os.mkdir(model_path)

            model_path_env = env_name
            model_path = os.path.join(model_path, model_path_env)
            if not os.path.exists(model_path):
                os.mkdir(model_path)

            model_path_setting = '_'.join(loginfo)
            model_path = os.path.join(model_path, model_path_setting)
            if not os.path.exists(model_path):
                os.mkdir(model_path)




            file_type={'qmodel' : 'qmodel' , 'mmodel' : 'mmodel' , 'dmodel' : 'dmodel'}
            file_path ={}
            for ii in file_type.iterkeys():

                file_path[ii] = os.path.join(model_path, file_type[ii])
                if not os.path.exists(file_path[ii]):
                    os.mkdir(file_path[ii])

            for ii in file_type.iterkeys():
                model_path_file = '_'.join([model_path_setting, str(self.total_steps / 10000)])+'.'+ii
                model_path = os.path.join(file_path[ii], model_path_file)
                if ii == 'qmodel':
                    torch.save(self.learning_network.state_dict(), model_path)
                elif ii =='mmodel':
                    torch.save(self.learning_network_m.state_dict(),model_path)
                elif ii == 'dmodel':
                    with open(model_path , 'wb') as f:
                        pickle.dump(self.density_model.get_state(),f,protocol=2)

            self.config.logger.info('save model done')


